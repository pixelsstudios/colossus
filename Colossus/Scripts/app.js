﻿calculateDenominations = function (input) {
    var denominationsNotes = [5000, 2000, 1000, 500];
    var denominationsCoins = [200, 100, 50, 20, 10, 5, 2, 1];
    var output = '';

    if (parseFloat(input) > 0)
    {
        input = parseFloat(input) * 100;

        for (var index = 0; index < denominationsNotes.length; index++)
        {
            if (input >= denominationsNotes[index])
            {
                var multiplier = (input - (input % denominationsNotes[index])) / denominationsNotes[index];
                output += multiplier + ' x £' + (denominationsNotes[index] / 100) + ' notes<br />';
                input = input - (multiplier * denominationsNotes[index]);
            }
        }
        for (var index = 0; index < denominationsCoins.length; index++) {
            if (input >= denominationsCoins[index]) {
                var multiplier = (input - (input % denominationsCoins[index])) / denominationsCoins[index];
                if (denominationsCoins[index] >= 100) {
                    output += multiplier + ' x £' + (denominationsCoins[index] / 100) + ' coins<br />';
                }
                else {
                    output += multiplier + ' x ' + denominationsCoins[index] + ' pence coins<br />';
                }
                input = input - (multiplier * denominationsCoins[index]);
            }
        }
    }

    document.getElementById('output').innerHTML = output;
};