﻿using Colossus.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Colossus.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        [HttpPost]
        public ActionResult Index(CalculatorVM model)
        {
            if (BitConverter.GetBytes(decimal.GetBits(model.Total)[3])[2] > 2)
            {
                ModelState.AddModelError("Total", "Only 2 decimal places are accepted");
            }

            if (ModelState.IsValid)
            {
                Collection<int> denominationsNotes = new Collection<int>() {
                    5000,
                    2000,
                    1000,
                    500 
                };

                Collection<int> denominationsCoins = new Collection<int>() {
                    200,
                    100,
                    50,
                    20,
                    10,
                    5,
                    2,
                    1
                };

                string output = "";

                int input = Convert.ToInt32(model.Total * 100);

                for (var index = 0; index < denominationsNotes.Count; index++)
                {
                    if (input >= denominationsNotes[index])
                    {
                        int multiplier = (input - (input % denominationsNotes[index])) / denominationsNotes[index];
                        output += multiplier + " x £" + (denominationsNotes[index] / 100) + " notes" + "<br />";
                        input = input - (multiplier * denominationsNotes[index]);
                    }
                }
                for (var index = 0; index < denominationsCoins.Count; index++) {
                    if (input >= denominationsCoins[index]) {
                        int multiplier = (input - (input % denominationsCoins[index])) / denominationsCoins[index];
                        if (denominationsCoins[index] >= 100) {
                            output += multiplier + " x £" + (denominationsCoins[index] / 100) + " coins" + "<br />";
                        }
                        else {
                            output += multiplier + " x " + denominationsCoins[index] + " pence coins" + "<br />";
                        }
                        input = input - (multiplier * denominationsCoins[index]);
                    }
                }

                ViewBag.Output = output;
            }
            return View ("Index", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
